import { Module } from '@nestjs/common';
import { BrokersService } from './brokers.service';

@Module({
  imports: [],
  providers: [BrokersService],
})
export class BrokersModule {}
