import { Injectable, Logger } from '@nestjs/common';
import random from '@jitesoft/random-string';

const MAX_ASSETS = 100;
const SYMBOL_LENGTH = 5;

@Injectable()
export class BrokersService {
  private readonly logger = new Logger(BrokersService.name);

  listTradableAssets(): Promise<Array<{ tickerSymbol: string }>> {
    const assets = [];
    for (let i = 0; i < MAX_ASSETS; i++)
      assets.push(
        random(SYMBOL_LENGTH, {
          special: false,
          numbers: false,
          alpha: true,
          minAlpha: 5,
          minNumbers: 0,
          minSpecial: 0,
        }).toUpperCase(),
      );
    this.logger.log('tradable assets generated', assets.join(','));
    return Promise.resolve(assets);
  }
}
