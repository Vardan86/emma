import { Module } from '@nestjs/common';
import { SymbolsController } from './symbols.controller';
import { SymbolsService } from './symbols.service';
import { Transport, ClientsModule } from '@nestjs/microservices';
import { BrokersModule } from 'src/broker/brokers.module';

@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'SYMBOLS_SERVICE',
        transport: Transport.KAFKA,
        options: {
          client: {
            clientId: 'symbols',
            brokers: ['kafka:9093'],
          },
          consumer: {
            groupId: 'symbols-consumer',
          },
        },
      },
    ]),
    BrokersModule,
  ],
  controllers: [SymbolsController],
  providers: [SymbolsService],
})
export class SymbolsModule {}
