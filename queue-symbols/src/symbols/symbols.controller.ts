import { Controller, Post } from '@nestjs/common';
import { SymbolsService } from './symbols.service';

@Controller()
export class SymbolsController {
  constructor(private readonly symbolsService: SymbolsService) {}

  @Post('/symbols')
  queueSymbols() {
    return this.symbolsService.queueSymbols();
  }
}
