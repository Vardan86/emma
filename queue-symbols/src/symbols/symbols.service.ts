import { Inject, Injectable, Logger, OnModuleInit } from '@nestjs/common';
import { ClientKafka } from '@nestjs/microservices';
import { BrokersService } from 'src/broker/brokers.service';

@Injectable()
export class SymbolsService implements OnModuleInit {
  private readonly logger = new Logger(SymbolsService.name);
  private readonly broker = new BrokersService();

  constructor(
    @Inject('SYMBOLS_SERVICE') private readonly client: ClientKafka,
  ) {}

  async onModuleInit() {
    this.client.subscribeToResponseOf('process.prices');
    await this.client.connect();
  }

  async queueSymbols() {
    const tradableAssets = await this.broker.listTradableAssets();
    for (const tradableAsset of tradableAssets) {
      await this.client
        // sendBatch is a better choice here
        .send('process.prices', tradableAsset)
        .toPromise();
      this.logger.log(`${tradableAsset} sent`);
    }
  }
}
