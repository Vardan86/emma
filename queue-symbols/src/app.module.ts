import { Module } from '@nestjs/common';
import { SymbolsModule } from './symbols/symbols.module';
import { BrokersModule } from './broker/brokers.module';

@Module({
  imports: [BrokersModule, SymbolsModule],
})
export class AppModule {}
