import { Body, Controller, Param, Post } from '@nestjs/common';
import { ClaimsService } from './claims.service';
import { ClaimDto } from './dto/claim.dto';

@Controller()
export class ClaimsController {
  constructor(private readonly claimService: ClaimsService) {}

  @Post('/claim-free-share')
  queueClaim(@Body() claimDto: ClaimDto) {
    return this.claimService.queueClaim(claimDto);
  }
}
