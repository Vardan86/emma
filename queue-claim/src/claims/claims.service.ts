import { Inject, Injectable, Logger, OnModuleInit } from '@nestjs/common';
import { ClientKafka } from '@nestjs/microservices';
import { ClaimDto } from './dto/claim.dto';

@Injectable()
export class ClaimsService implements OnModuleInit {
  private readonly logger = new Logger(ClaimsService.name);

  constructor(
    @Inject('CLAIM_SERVICE') private readonly client: ClientKafka,
  ) {}

  async onModuleInit() {
    this.client.subscribeToResponseOf('process.claims');
    await this.client.connect();
  }

  async queueClaim(claimDto: ClaimDto) {
    const { accountId } = claimDto;
    await this.client
      .send('process.claims', accountId)
      .toPromise();
    this.logger.log(`${accountId} processed`);
  }
}
