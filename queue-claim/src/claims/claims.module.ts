import { Module } from '@nestjs/common';
import { ClaimsController } from './claims.controller';
import { ClaimsService } from './claims.service';
import { Transport, ClientsModule } from '@nestjs/microservices';

@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'CLAIM_SERVICE',
        transport: Transport.KAFKA,
        options: {
          client: {
            clientId: 'claims',
            brokers: ['kafka:9093'],
          },
          consumer: {
            groupId: 'claims-consumer',
          },
        },
      },
    ]),
  ],
  controllers: [ClaimsController],
  providers: [ClaimsService],
})
export class ClaimsModule {}
