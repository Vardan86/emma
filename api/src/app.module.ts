import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PriceModule } from './prices/price.module';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: process.env.PG_HOST || 'pg',
      port: Number(process.env.PG_PORT) || 5432,
      username: process.env.PG_USER || 'user',
      password: process.env.PG_PASS || 'password',
      database: process.env.PG_DB || 'stocks',
      autoLoadEntities: true,
      synchronize: true,
    }),
    PriceModule,
  ],
})
export class AppModule {}
