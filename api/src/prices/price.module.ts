import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Price } from './price.entity';
import { PricesController } from './price.controller';
import { PricesService } from './price.service';

@Module({
  imports: [TypeOrmModule.forFeature([Price])],
  providers: [PricesService],
  controllers: [PricesController],
})
export class PriceModule {}
