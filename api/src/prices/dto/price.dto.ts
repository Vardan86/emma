export class PriceDto {
  symbol: string;
  price: number;
}
