import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Between, MoreThan, Repository } from 'typeorm';
import { PriceDto } from './dto/price.dto';
import { Price } from './price.entity';
import { DateTime } from 'luxon';

const REFRESH_HOURS = 24;

@Injectable()
export class PricesService {
  private readonly logger = new Logger(PricesService.name);
  constructor(
    @InjectRepository(Price)
    private readonly pricesRepository: Repository<Price>,
  ) {}

  createOrUpdate(priceDto: PriceDto): Promise<Price> {
    const price = new Price();
    price.price = priceDto.price;
    price.symbol = priceDto.symbol;
    this.logger.log(`saving ${price.symbol} for ${price.price}`);
    return this.pricesRepository.save(price);
  }

  async getSymbolByPrice(min: number, max: number): Promise<Price> {
    this.logger.log(`Symbol requested between ${min} and ${max}`);
    return this.pricesRepository.findOne({
      where: {
        price: Between(min, max),
        updated_at: MoreThan(
          DateTime.local().minus({
            hours: REFRESH_HOURS,
          }),
        ),
      },
    });
  }
}
