import { Body, Controller, Get, Logger, Post, Query } from '@nestjs/common';
import { PriceDto } from './dto/price.dto';
import { Price } from './price.entity';
import { PricesService } from './price.service';

@Controller('prices')
export class PricesController {
  constructor(private readonly pricesService: PricesService) {}

  @Post()
  create(@Body() priceDto: PriceDto): Promise<Price> {
    return this.pricesService.createOrUpdate(priceDto);
  }

  @Get()
  getSymbolByPrice(
    @Query('min') min: number,
    @Query('max') max: number,
  ): Promise<Price> {
    return this.pricesService.getSymbolByPrice(min, max);
  }
}
