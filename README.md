# Buy stocks demo

this is simple implementation of message driven stock purchase
there are 4 services

`queue-symbols` - to query symbols from stock and stream it to queue

`symbols-to-prices` - to process the symbol stream and query the price of each and send it to price cache db

`api` - prices api (caching to db)

`queue-claim` - claim free stock (stream it to kafka)

`process-claim` - process the claims stream by retrieving the prices from `api` and calling mocked broker service methods


![Structure](structure.png)

# how to run & test

To start the docker compose script

`docker-compose up`

To
run the symbol caching

`curl -XPOST http://localhost:3002/symbols`

to add a claim

`curl -XPOST http://localhost:3003/claim-free-share -d '{"accountId":"myaccount"}'    -H 'Content-Type: application/json'`  

