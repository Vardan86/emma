import { Injectable, Logger } from '@nestjs/common';
import { randomSync } from 'pure-random-number';

const MIN_PRICE = process.env.MIN_PRICE || 1;
const MAX_PRICE = process.env.MAX_PRICE || 300;

@Injectable()
export class BrokersService {
  private readonly logger = new Logger(BrokersService.name);

  // assuming market open status isn't important
  async getLatestPrice(symbol: string): Promise<{ sharePrice: number }> {
    const price = await randomSync(MIN_PRICE, MAX_PRICE);
    this.logger.log(`price for ${symbol} is ${price} `);
    return price;
  }
}
