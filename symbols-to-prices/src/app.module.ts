import { Module } from '@nestjs/common';
import { PricesModule } from './price/prices.module';
import { BrokersModule } from './broker/brokers.module';

@Module({
  imports: [BrokersModule, PricesModule],
})
export class AppModule {}
