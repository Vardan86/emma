import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    AppModule,
    {
      transport: Transport.KAFKA,
      options: {
        client: {
          clientId: 'symbols',
          brokers: ['kafka:9093'],
        },
        consumer: {
          groupId: 'symbols-consumer',
        },
      },
    },
  );
  app.listen(null);
}
bootstrap();
