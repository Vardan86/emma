import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { BrokersModule } from 'src/broker/brokers.module';
import { PricesController } from './prices.controller';

@Module({
  imports: [BrokersModule, HttpModule],
  controllers: [PricesController],
  providers: [],
})
export class PricesModule {}
