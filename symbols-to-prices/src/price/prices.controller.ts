import { Controller, Logger } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { PricesService } from './prices.service';

@Controller()
export class PricesController {
  private readonly logger = new Logger(PricesController.name);
  private readonly prices = new PricesService();

  @MessagePattern('process.prices')
  async onSymbolReceived(@Payload() message) {
    const { value: symbol } = message;
    await this.prices.processSymbols(symbol);
    this.logger.log(`${symbol} processed`);
  }
}
