import { Injectable, Logger, HttpService } from '@nestjs/common';
import { BrokersService } from 'src/broker/brokers.service';

const API_URL = process.env.API_URL || 'http://api:3001/prices';

@Injectable()
export class PricesService {
  private readonly logger = new Logger(PricesService.name);
  private readonly broker = new BrokersService();
  private readonly httpService = new HttpService();

  async processSymbols(symbol) {
    const price = await this.broker.getLatestPrice(symbol);
    await this.httpService
      .post(API_URL, {
        symbol,
        price,
      })
      .toPromise();
    this.logger.log(`${symbol} posted with price of ${price} to API`);
  }
}
