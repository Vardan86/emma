import { Injectable, Logger } from '@nestjs/common';
import { randomSync } from 'pure-random-number';
import { DateTime } from 'luxon';

const FAKE_OPENING_DELAY_SECONDS = 5;

// this should be extracted
export class MarketClosedError extends Error {
  constructor(message) {
    super(message);
  }
}

@Injectable()
export class BrokersService {
  private readonly logger = new Logger(BrokersService.name);

  async buySharesInRewardsAccount(
    tickerSymbol: string,
    quantity: number,
  ): Promise<{ success: boolean; sharePricePaid: number }> {
    this.logger.log(`buySharesInRewardsAccount ${tickerSymbol} ${quantity}`);
    // we could try to make custom type but lets keep it simple
    if (!Number.isInteger(quantity)) {
      throw new Error('Invalid quantity');
    }
    const isMarketOpen = await this.isMarketOpen();
    if (!isMarketOpen) {
      throw new MarketClosedError('Market is closed, try again later');
    }
    // not sure if the actual price data needs to be retrieved or just mock data should be provided
    return {
      success: true,
      sharePricePaid: randomSync(1, 300),
    };
  }

  async isMarketOpen(): Promise<{
    open: boolean;
    nextOpeningTime?: string;
    nextClosingTime?: string;
  }> {
    const rand = randomSync(1, 10);
    // randomly emulate closed market
    if (rand % 2) {
      return {
        open: true,
      };
    } else {
      return {
        open: false,
        nextOpeningTime: DateTime.local().plus({
          seconds: FAKE_OPENING_DELAY_SECONDS,
        }),
      };
    }
  }
  // not sure if this should be fully mocked
  moveSharesFromRewardsAccount(
    toAccount: string,
    tickerSymbol: string,
    quantity: number,
  ): Promise<{ success: boolean }> {
    return Promise.resolve({ success: true });
  }
}
