import { Module } from '@nestjs/common';
import { ClaimsModule } from './claims/claims.module';
import { BrokersModule } from './broker/brokers.module';

@Module({
  imports: [BrokersModule, ClaimsModule],
})
export class AppModule {}
