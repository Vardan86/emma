import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    AppModule,
    {
      transport: Transport.KAFKA,
      options: {
        client: {
          clientId: 'claims',
          brokers: ['kafka:9093'],
        },
        consumer: {
          groupId: 'claims-consumer',
        },
      },
    },
  );
  app.listen(null);
}
bootstrap();
