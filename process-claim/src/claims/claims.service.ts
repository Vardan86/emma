import { Injectable, Logger, HttpService } from '@nestjs/common';
import { BrokersService, MarketClosedError } from 'src/broker/brokers.service';
import { randomSync } from 'pure-random-number';
import { waitUntil } from 'async-wait-until';

const API_URL = process.env.API_URL || 'http://api:3001/prices';
const MIN_PRICE = process.env.MIN_PRICE || 3;
const MAX_PRICE = process.env.MAX_PRICE || 200;
@Injectable()
export class ClaimsService {
  private readonly logger = new Logger(ClaimsService.name);
  private readonly broker = new BrokersService();
  private readonly httpService = new HttpService();
  priceRange() {
    const rand = randomSync(0, 100);
    if (rand <= 95) {
      return { min: MIN_PRICE, max: 10 };
    } else if (rand <= 98) {
      return { min: 10, max: 25 };
    } else {
      return { min: 24, max: MAX_PRICE };
    }
  }
  async processClaims(accountId) {
    const { min, max } = this.priceRange();
    const {
      data: { symbol },
    } = await this.httpService
      .get<{ symbol: string }>(API_URL, {
        params: { min, max },
      })
      .toPromise();
    try {
      await this.broker.buySharesInRewardsAccount(symbol, 1);
    } catch (error) {
      if (error instanceof MarketClosedError) {
        // wait for market to open (for FaaS we would implement scheduler instead)
        const { nextOpeningTime } = await this.broker.isMarketOpen();
        this.logger.log(`Markets closed will wait till ${nextOpeningTime}`);
        await waitUntil(() => Date.now() >= Date.parse(nextOpeningTime), {
          timeout: 1000 * 3600,
        });
        this.logger.log(`Markets are expected to open now`);
        return this.processClaims(accountId);
      } else {
        // todo: handling
        this.logger.log(`Something wrong for claim with ${accountId}`);
      }
    }
    // skipping getRewardsAccountPositions as idk why do we need it
    // buying 1 since we suppose there are always available stocks
    await this.broker.moveSharesFromRewardsAccount(accountId, symbol, 1);
  }
}
