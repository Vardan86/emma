import { Controller, Logger } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { ClaimsService } from './claims.service';

@Controller()
export class ClaimsController {
  private readonly logger = new Logger(ClaimsController.name);
  private readonly claims = new ClaimsService();

  // skipping the dto
  @MessagePattern('process.claims')
  async onClaimReceived(@Payload() message) {
    const { value: accountId } = message;
    this.logger.log(`processing claim for accountid ${accountId}`);
    await this.claims.processClaims(accountId);
    this.logger.log(`${accountId} processed`);
  }
}
