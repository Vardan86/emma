import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { BrokersModule } from 'src/broker/brokers.module';
import { ClaimsController } from './claims.controller';

@Module({
  imports: [BrokersModule, HttpModule],
  controllers: [ClaimsController],
  providers: [],
})
export class ClaimsModule {}
